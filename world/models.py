#from django.db import models
from django.contrib.gis.db import models

class Waterbody(models.Model):
    id = models.IntegerField(primary_key=True)
    lat = models.FloatField()
    long = models.FloatField()
    country = models.CharField(max_length=100)
    name = models.CharField(max_length=100, unique=True)
    max_level_total = models.FloatField(default=0.0)
    nominal_outline = models.PolygonField(geography=True, blank=True, null=True)
class Measurement(models.Model):
    Waterbody = models.ForeignKey(Waterbody, on_delete=models.CASCADE)
    cc = models.FloatField()
    date = models.DateField()
    level = models.FloatField()
    max_level = models.FloatField(default=0.0)
    min_level = models.FloatField(default=0.0)
    outline = models.MultiPolygonField(geography=True, blank=True, null=True)