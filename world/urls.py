from os import name
from django.urls import path

from . import views
from . import views

urlpatterns = [
    # View to create patch
    path('waterbodies/', views.waterbodies, name='waterbodies'),
    path('waterbodies/<int:waterbody_id>/', views.waterbody_details, name='waterbody_details'),
    path('waterbodies/<int:waterbody_id>/measurements/<str:date>/', views.measurements, name='measurements')
]
