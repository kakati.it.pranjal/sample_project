import json
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
from .models import Waterbody, Measurement

def waterbodies(request):
    # waterbodies_list = Waterbody.objects.all().values('id', 'country', 'lat', 'long', 'name')

    # return HttpResponse(json.dumps(list(waterbodies_list), cls=DjangoJSONEncoder), content_type="application/json")

    waterbodies_json = f'waterbodies/all.json'
    with open(waterbodies_json) as waterbody_file:
        response_data = json.load(waterbody_file)

    return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

def waterbody_details(request, waterbody_id):
    # response_data['max_level_total'] = Waterbody.objects.get(id=waterbody_id).values('max_level_total')
    # response_data['measurements'] = list(Measurement.objects
    #                                                 .get(id=waterbody_id)
    #                                                 .values('cc', 'date', 'level', 'max_level', 'min_level')
    #                                                 )
    # response_data['nominal_outline'] = Waterbody.objects.get(id=waterbody_id).values('nominal_outline')
    # response_data['properties'] = Waterbody.objects.get(id=waterbody_id).values('country', 'id', 'lat', 'long', 'name')
    # return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")
    waterbody_json = f'waterbodies/{waterbody_id}/nominal.json'
    with open(waterbody_json) as waterbody_file:
        response_data = json.load(waterbody_file)

    return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")

def measurements(request, waterbody_id, date):
    measurement_json = f'waterbodies/{waterbody_id}/{date}.json'
    with open(measurement_json) as measurement_file:
        response_data = json.load(measurement_file)

    return HttpResponse(json.dumps(response_data, cls=DjangoJSONEncoder), content_type="application/json")