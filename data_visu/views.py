# Create your views here.
import os
import os.path
import sys
from django.views.decorators.csrf import csrf_exempt
import json
from pprint import pprint

import uuid

import numpy as np
# import matplotlib.pyplot as plt

import pyproj
from PIL import Image
# Create your views here.
from django.http import HttpResponse, FileResponse
from django.template import loader
# from matplotlib import cm
from numpy import integer, ma, source
from osgeo import gdal, ogr
from osgeo import osr
# import matplotlib.colors as colors
import geojson
from geojson import Feature, Polygon, FeatureCollection
from rasterio.features import rasterize
#import buzzard as buzz

from data_visu.constants import input_data_path

def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render())
def view_image_sentinel_2(request):
    image_file = request.GET.get('image_file', '')
    crs = pyproj.Proj(init='EPSG:3857')

    first = int(request.GET.get('band1', ''))
    second = int(request.GET.get('band2', ''))
    third = int(request.GET.get('band3', ''))

    bands = [first, second, third]

    image_file = input_data_path + os.sep + os.path.basename(image_file)
    data_array = gdal.Open(image_file)
    ds = gdal.BuildVRT('', data_array, resolution='highest', srcNodata=0, bandList=bands)

    print(ds)
    if (request.GET.get('bbox', '') != ''):
        BBOX = request.GET.get('bbox', '')
    else:
        BBOX = request.GET.get('BBOX', '')
    request_bbox = [float(t) for t in BBOX.split(',')]

    inSRS_wkt = data_array.GetProjection()  # gives SRS in WKT
    inSRS_converter = osr.SpatialReference()  # makes an empty spatial ref object
    inSRS_converter.ImportFromWkt(inSRS_wkt)  # populates the spatial ref object with our WKT SRS
    proj = inSRS_converter.ExportToProj4()  # Exports an SRS ref as a Proj4 string usable by PyProj

    request_bbox[0], request_bbox[1] = pyproj.transform(crs, proj, request_bbox[0], request_bbox[1])
    request_bbox[2], request_bbox[3] = pyproj.transform(crs, proj, request_bbox[2], request_bbox[3])
    out = gdal.Translate('', ds, format='MEM', strict=True, width=256, height=256,
                         projWin=[request_bbox[0], request_bbox[3], request_bbox[2], request_bbox[1]])
    vis = np.dstack(
        (out.ReadAsArray()[0, ...] / 4000, out.ReadAsArray()[1, ...] / 4000, out.ReadAsArray()[2, ...] / 4000))
    print(vis.shape)
    vis[vis > 1] = 1
    if (np.max(vis) != 0):
        vis11 = [vis > 0]
        vis12 = np.array(vis11)
        bool_val = np.multiply(vis12[0, :, :, 0], 1)
        test = np.dstack((vis, bool_val))
        img = Image.fromarray(np.uint8(test * 255))
        response = HttpResponse(content_type="image/png")
        img.save(response, "PNG")
        return response
    else:
        img = Image.new('RGBA', (256, 256), (0, 0, 0, 0))
        response = HttpResponse(content_type="image/png")
        img.save(response, "PNG")
        return response

@csrf_exempt
def subtract(request):
    geo = json.loads(request.body)    
    id = uuid.uuid4()
    input = f'output/{id}.geojson'

    # Writing to geojson file
    with open(input, "w") as outfile:
        outfile.write(json.dumps(geo, indent = 4))

    # model output
    base = 'data/sentinel-2/ouptut_sample/S2A_MSIL1C_20201122T043111_N0209_R133_T45RYH_20201122T055016.SAFE_results.tif'

    # Make copy of base before buring to avoid overwrite and get an id
    out = f'output/{id}.tif'
    os.system(f'cp {base} {out}')
    os.system(f'gdal_rasterize -b 1 -burn 0 {input} {out}')

    response_data = {}
    response_data['id'] = f'{id}.tif'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

@csrf_exempt
def make_raster(request):
    geo = json.loads(request.body)    
    id = uuid.uuid4()
    input = f'output/{id}.geojson'

    # Writing to geojson file
    with open(input, "w") as outfile:
        outfile.write(json.dumps(geo, indent = 4))

    annotated = f'output/{id}_ann.tif'
    base = 'data/sentinel-2/ouptut_sample/S2A_MSIL1C_20201122T043111_N0209_R133_T45RYH_20201122T055016.SAFE_results.tif'
    raster = gdal.Open(base)

    gt = raster.GetGeoTransform()
    os.system(f'rio rasterize {annotated} --res {gt[1]} < {input} --overwrite')
    os.system(f'gdal_calc.py -A {base} -B {annotated} --calc="maximum(A,B)" --extent="union" --outfile output/{id}.tif')

    response_data = {}
    response_data['id'] = f'{id}.tif'
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def download_file(request):
    response = FileResponse(open(f"output/{request.GET['file']}", 'rb'))
    return response