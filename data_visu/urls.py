from django.urls import path

from . import views

urlpatterns = [
    # View to create patch
    path('', views.index, name='index'),
    path('view_image_sentinel_2', views.view_image_sentinel_2, name='view_image_sentinel_2'),
    path('make_raster', views.make_raster, name='make_raster'),
    path('subtract', views.subtract, name='subtract'),
    path('download', views.download_file, name='download')
]
